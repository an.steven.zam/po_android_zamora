package facci.pm.ta2.poo.pra1;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import facci.pm.ta2.poo.datalevel.DataException;
import facci.pm.ta2.poo.datalevel.DataObject;
import facci.pm.ta2.poo.datalevel.DataQuery;
import facci.pm.ta2.poo.datalevel.GetCallback;

public class DetailActivity extends AppCompatActivity {
    TextView precio, descripcion,nombre;
    ImageView img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);


        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("PR1 :: Detail");


        // INICIO - CODE6
        //3.1
        //Crea una instancia de la clase Intent para llamar a la activity DetailActivity y pasa
        // como un parametro el m_objectID del DataObject

        String object= getIntent().getStringExtra("object_id");
        nombre = (TextView) findViewById(R.id.nombre);
        descripcion = (TextView) findViewById(R.id.descripcion);
        precio = (TextView) findViewById(R.id.precio);
        img = (ImageView) findViewById(R.id.thumbnail);

        // definimos las linea de acceso al object id
        /////////////////
        // PREGUNTA 3.3
        final DataQuery query = DataQuery.get("item");
        String parametro = getIntent().getExtras().getString("dato");
        query.getInBackground(parametro, new GetCallback<DataObject>() {
            @Override
            public void done(DataObject object, DataException e) {
                if (e == null){

                    //llamamos los datos etablecidos en la app
                    //Recibe a las propiedades del object del tipo String
                    String Precio = (String) object.get("price");
                    String Descripcion = (String) object.get("description");
                    String Nombre = (String) object.get("name");
                    Bitmap bitmap =(Bitmap) object.get("image");
                    //mostramos  la convinacion de variable
                    //se guardan los datos
                    precio.setText(Precio);
                    precio.setTextColor(getColor(R.color.precioRojo));
                    descripcion.setText(Descripcion);
                    nombre.setText(Nombre);
                    img.setImageBitmap(bitmap);
                }else{
                    //error
                } }
        });

         // FIN - CODE6

    }

}
